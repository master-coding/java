package com.mastercoding.fullstack.java.strings;

public class StringExample {
    public static void main(String[] args) {
        String s1 = "abc";
        String s2 = "abc";

        String s3 = new String("abc");
        String s4 = new String("abc");
        String s5 = new String("abc");

        if(s1.equals(s2)){
            System.out.println("s1 and s2 are same");
        }

        if(s1.equals(s3)){
            System.out.println("s1 and s3 are same");
        }

        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);

        System.out.println(s1.charAt(2));
        System.out.println(s1.concat("def"));

        System.out.println("we are in class".endsWith("class"));
        System.out.println("we are in class".indexOf("a"));
        System.out.println("".isEmpty());
        System.out.println("we are in class".replace("a", "d"));

        String[] words = "we are in class".split(" ");
        System.out.println(words[0]);
        System.out.println(words[1]);
        System.out.println(words[2]);
        System.out.println(words[3]);

        String[] words1 = "we,are,in,class".split(",");
        System.out.println(words1[0]);
        System.out.println(words1[1]);
        System.out.println(words1[2]);
        System.out.println(words1[3]);

        System.out.println("we are in class".substring(3));
        System.out.println("     we are in class   ".trim());

    }
}
