package com.mastercoding.fullstack.java.strings;

import java.util.StringTokenizer;

public class StringBuilderExample {
    public static void main(String[] args) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("abc");
        buffer.append("def");
        buffer.append("lak");

        StringBuilder builder = new StringBuilder();
        builder.append("abc");
        builder.append("def");
        builder.append("lak");

        System.out.println(buffer.toString());
        System.out.println(builder.toString());

        StringTokenizer tokenizer = new StringTokenizer("we are in class");
        System.out.println(tokenizer.countTokens());
        while (tokenizer.hasMoreTokens()){
            System.out.println(tokenizer.nextToken());
        }

    }
}
