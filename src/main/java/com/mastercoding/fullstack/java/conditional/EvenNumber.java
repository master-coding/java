package com.mastercoding.fullstack.java.conditional;

public class EvenNumber {
    public static void main(String[] args) {
        int number = 10;
        if(number % 2 == 0) {
            System.out.println("The number is even");
        }else{
            System.out.println("odd");
        }
    }
}
