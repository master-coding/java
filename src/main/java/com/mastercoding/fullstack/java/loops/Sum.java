package com.mastercoding.fullstack.java.loops;

import java.util.Scanner;

public class Sum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextInt();
        int sum = 0;
        int i = 1;
        while (i <= n){
            sum = sum + i;
            i = i+1;
        }

        System.out.println("Sum = " + sum);
    }
}
