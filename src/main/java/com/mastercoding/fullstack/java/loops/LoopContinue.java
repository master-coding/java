package com.mastercoding.fullstack.java.loops;

import java.util.Scanner;

public class LoopContinue {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for(int i=0; i < n; i++){
            System.out.println("------");
            System.out.println("before if : " + i);
            if(i % 2 == 0){
                continue;
            }
            System.out.println("after if : " + i);
        }
    }
}
