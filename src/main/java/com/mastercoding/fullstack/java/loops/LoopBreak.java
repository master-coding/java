package com.mastercoding.fullstack.java.loops;

import java.util.Scanner;

public class LoopBreak {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for(int i = n; i <= n+9; i++){
            System.out.println("checking ... " + i);
                if(i % 9 == 0){
                    System.out.println(i);
                    break;
                }
        }
    }
}
