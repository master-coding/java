package com.mastercoding.fullstack.java.loops;

import java.util.Scanner;

public class StarPatternTwoLoops {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        for (int x = 1; x <= n; x++) {
            for (int i = 1; i <= x; i++) {
                System.out.print("* ");
            }
            System.out.println("");
        }

    }
}
