package com.mastercoding.fullstack.java.loops;

import java.util.Scanner;

public class EvenSum1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int sum = 0;
        int i = 0;

        while (i <= n){
            sum = sum + i;
            i = i + 2;
        }
        System.out.println("Sum = " + sum);
    }
}
