package com.mastercoding.fullstack.java.loops;

import java.util.Scanner;

public class EvenSum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int sum = 0;
        int i = 1;

        while (i <= n){
            if( i % 2 == 0) {
                sum = sum + i;
            }
            i = i + 1;
        }
        System.out.println("Sum = " + sum);
    }
}
