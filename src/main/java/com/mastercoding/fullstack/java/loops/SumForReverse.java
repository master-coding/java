package com.mastercoding.fullstack.java.loops;

import java.util.Scanner;

public class SumForReverse {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int sum = 0;
        for (int i = n; i >= 0; i--) {
            sum = sum + i;
        }
        System.out.println(sum);
    }
}
