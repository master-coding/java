package com.mastercoding.fullstack.java.loops;

import java.util.Scanner;

public class SumDoWhile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int i = 0, sum = 0;
        do {
            sum = sum + i;
            i = i + 1;
        }while (i <= n);
        System.out.println(sum);
    }
}
