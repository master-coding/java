package com.mastercoding.fullstack.java.collections;

import java.util.HashSet;
import java.util.LinkedHashSet;

public class HashSetExample {
    public static void main(String[] args) {
        HashSet<String> employeeSet = new HashSet<>();
        employeeSet.add("RAM");
        employeeSet.add("LAKSHMAN");
        employeeSet.add("BHARATH");
        employeeSet.add("JOHN");

        employeeSet.remove("JOHN");

        System.out.println(employeeSet.contains("RAM"));
        System.out.println(employeeSet.contains("BOB"));
        System.out.println(employeeSet.contains("JOHN"));

        System.out.println("---------");

        for(String name : employeeSet){
            System.out.println(name);
        }

        System.out.println("---------");
        LinkedHashSet<String> employeeOrderedSet = new LinkedHashSet<>();
        employeeOrderedSet.add("RAM");
        employeeOrderedSet.add("LAKSHMAN");
        employeeOrderedSet.add("BHARATH");

        for(String name : employeeOrderedSet){
            System.out.println(name);
        }

    }
}
