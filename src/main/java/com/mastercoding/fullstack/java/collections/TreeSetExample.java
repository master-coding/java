package com.mastercoding.fullstack.java.collections;

import java.util.TreeSet;

public class TreeSetExample {
    public static void main(String[] args) {
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(10);
        treeSet.add(100);
        treeSet.add(1);
        treeSet.add(50);
        treeSet.add(3);
        treeSet.add(19);

        System.out.println(treeSet.contains(50));
        System.out.println(treeSet.contains(3));
        System.out.println(treeSet.contains(98));

        for(int i : treeSet){
            System.out.print(i + " ");
        }


    }
}
