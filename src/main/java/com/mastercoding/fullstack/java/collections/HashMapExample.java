package com.mastercoding.fullstack.java.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class HashMapExample {
    public static void main(String[] args) {
        HashMap<String, String> peopleMap = new HashMap<>();
        peopleMap.put("RAM", "1111");
        peopleMap.put("RAM", "1112");
        peopleMap.put("LAKSHMAN", "2222");
        peopleMap.put("JOHN", "3333");
        peopleMap.put("BOB", "4444");

        System.out.println(peopleMap.get("RAM"));
        System.out.println(peopleMap.get("JOHN"));
        System.out.println(peopleMap.get("BHARATH"));
        System.out.println(peopleMap.containsKey("BHARATH"));

        System.out.println("---------------");

        HashMap<String, List<String>> peopleMap1 = new HashMap<>();
        ArrayList<String> ramPhoneNumbers = new ArrayList<>();
        ramPhoneNumbers.add("1111");
        ramPhoneNumbers.add("1112");
        peopleMap1.put("RAM", ramPhoneNumbers);

        LinkedList<String> lakshmanNumbers = new LinkedList<>();
        lakshmanNumbers.add("2222");
        lakshmanNumbers.add("2223");
        peopleMap1.put("LAKSHMAN", lakshmanNumbers);



    }
}
