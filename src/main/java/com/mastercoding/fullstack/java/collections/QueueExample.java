package com.mastercoding.fullstack.java.collections;

import java.util.LinkedList;
import java.util.Queue;

public class QueueExample {
    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();
        Integer first = queue.poll();
        System.out.println(first);

//        Integer first1 = queue.remove();
//        System.out.println(first1);

        queue.add(10);
        queue.add(20);
        queue.add(30);
        queue.clear();

        queue.clear();

        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.poll());

        LinkedList<Integer> queue1 = new LinkedList<>();
        queue1.add(10);
        queue1.add(20);
        queue1.add(30);
        System.out.println(queue1.poll());
        queue1.clear();
        System.out.println(queue1.poll());
    }
}
