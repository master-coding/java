package com.mastercoding.fullstack.java.collections;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeSet;

public class ArrayListExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        int[] arr = new int[5];
        ArrayList<Integer> arrayList = new ArrayList<>(1000);
        for(int i =0; i < 100; i++){
            arrayList.add(i);
        }

        arrayList.add(20, 2);

        System.out.println(arrayList.get(9));
        arrayList.remove(9);
        System.out.println(arrayList.get(9));

        System.out.println("------------------");
        for(int num : arrayList){
            System.out.println(num);
        }
    }
}
