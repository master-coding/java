package com.mastercoding.fullstack.java.collections;

import java.util.ArrayList;
import java.util.LinkedList;

public class LinkedListExamples {
    public static void main(String[] args) {
        LinkedList<Float> list = new LinkedList<Float>();
        list.add(10.1f);
        list.add(20.2f);
        list.add(30.3f);

        ArrayList<Float> arrayList = new ArrayList<>();
        arrayList.addAll(list);

        System.out.println(list.get(2));
    }
}
