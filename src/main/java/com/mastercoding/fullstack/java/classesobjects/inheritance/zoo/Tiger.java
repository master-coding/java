package com.mastercoding.fullstack.java.classesobjects.inheritance.zoo;

public class Tiger extends Animal{
    @Override
    public void makeSound() {
        System.out.println("Tiger is roaring");
    }
}
