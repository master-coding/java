package com.mastercoding.fullstack.java.classesobjects;

public class HumanTest {
    public static void main(String[] args) {
        Human ram = new Human("ram", 60f, 5.4f, "white");
        ram.walk();
        ram.listen();
        ram.updateName("Rammohan");

        Human lakshman = new Human("lakshman", 70f, 5.9f, "brown");
        lakshman.walk();
        lakshman.listen();
    }
}
