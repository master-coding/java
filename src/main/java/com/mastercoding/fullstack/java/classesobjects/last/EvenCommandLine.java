package com.mastercoding.fullstack.java.classesobjects.last;

public class EvenCommandLine {
    public static void main(String[] args) {
        int num = Integer.parseInt(args[0]);
        if(num % 2 == 0){
            System.out.println(num + " is a even number");
        }else {
            System.out.println(num + " is a odd number");
        }
    }
}
