package com.mastercoding.fullstack.java.classesobjects.bank;

public class BankAccountTest {
    public static void main(String[] args) {
        BankAccount ramAccount = new BankAccount("Ram", "ACC-1", 5000);
        ramAccount.deposit(400);
        ramAccount.withdraw(2000);
        ramAccount.deposit(1000);
        ramAccount.print();

        BankAccount lakshmanAccount = new BankAccount("Lakshman", "ACC-1", 10000);
        lakshmanAccount.deposit(1000);
        lakshmanAccount.withdraw(500);
        lakshmanAccount.print();

    }
}
