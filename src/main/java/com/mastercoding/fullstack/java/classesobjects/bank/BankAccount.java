package com.mastercoding.fullstack.java.classesobjects.bank;

public class BankAccount {
    private double balance;
    private String accountHolder;
    private String accountId;
    private int numTransactions = 0;
    private static int totalTransaction = 0;

    //opening account with some balance
    public BankAccount(String name, String id, double balance){
        this.accountHolder = name;
        this.accountId = id;
        this.balance = balance;
    }

    //zero balance account
    public BankAccount(String name, String id){
        this.accountHolder = name;
        this.accountId = id;
        this.balance = 0;
    }

    public void deposit(double money){
        balance = balance + money;
        numTransactions++;
        totalTransaction++;
    }

    public void withdraw(double money) {
        balance = balance - money;
        numTransactions++;
        totalTransaction++;
    }

    public void print(){
        System.out.println("Account Name : " + accountHolder);
        System.out.println("Account ID : " + accountId);
        System.out.println("Balance : " + balance);
        System.out.println("Number of transactions : " + numTransactions);
        System.out.println("Total number of transactions : " + totalTransaction);
        System.out.println("-----------------");
    }
}
