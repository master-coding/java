package com.mastercoding.fullstack.java.classesobjects.abstraction.room;

public class SwitchMain {
    public static void main(String[] args) {
        Light light = new Light();
        Fan fan = new Fan();

        Switch switch1 = new Switch();
        switch1.connect(light);
        switch1.on();
    }
}
