package com.mastercoding.fullstack.java.classesobjects.inheritance.vehicles;

public class Bus extends Vehicle{
    @Override
    public void move() {
        System.out.println("Bus is moving");
    }
}
