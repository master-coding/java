package com.mastercoding.fullstack.java.classesobjects.abstraction.room;

public class Switch {
    private ElectricalDevice device;

    public void connect(ElectricalDevice device){
        this.device = device;
    }

    public void on(){
        if(device != null){
            device.on();
        }
    }

    public void off(){
        if(device != null){
            device.off();
        }
    }
}
