package com.mastercoding.fullstack.java.classesobjects.accessspecifiers.p1;

public class Animal {
    private String name = "Lion";
    public String color = "yellow";
    protected int noOfLegs = 4;
    float weight = 100;

    private void printName() {
        System.out.println("Name : " + name);
    }

    public void printColor() {
        System.out.println("Color : " + color);
    }

    protected void printLegs() {
        System.out.println("No. of Legs : " + noOfLegs);
    }

    void printWight() {
        System.out.println(weight);
    }

}
