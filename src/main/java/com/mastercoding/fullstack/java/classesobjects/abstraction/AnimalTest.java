package com.mastercoding.fullstack.java.classesobjects.abstraction;

public class AnimalTest {
    public static void main(String[] args) {
        Lion lion = new Lion();
        Tiger tiger = new Tiger();
        Fox fox = new Fox();

        Animal[] animals = new Animal[3];
        animals[0] = lion;
        animals[1] = tiger;
        animals[2] = fox;



        for (int i=0; i < animals.length; i++){
            animals[i].makeSound();
            animals[i].run();
            animals[i].walk();
        }

    }
}
