package com.mastercoding.fullstack.java.classesobjects.inheritance.zoo;

public class Animal {
    public void makeSound() {
        System.out.println("Animal is making sound");
    }

    public void walk() {
        System.out.println("Animal is walking");
    }
}
