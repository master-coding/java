package com.mastercoding.fullstack.java.classesobjects.accessspecifiers.p2;

import com.mastercoding.fullstack.java.classesobjects.accessspecifiers.p1.Animal;

public class AnimalTest2 {
    public static void main(String[] args) {
        Animal animal = new Animal();
        System.out.println(animal.color);
        animal.printColor();

        Lion lion = new Lion();

    }
}
