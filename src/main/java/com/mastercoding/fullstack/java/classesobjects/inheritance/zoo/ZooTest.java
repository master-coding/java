package com.mastercoding.fullstack.java.classesobjects.inheritance.zoo;

public class ZooTest {
    public static void main(String[] args) {
        Lion lion1 = new Lion();
        Tiger tiger1 = new Tiger();
        Dog dog1 = new Dog();
        Hyena hyena1 = new Hyena();

        Animal[] animals = new Animal[4];
        animals[0] = lion1;
        animals[1] = tiger1;
        animals[2] = dog1;
        animals[3] = hyena1;

        //generic cod for all animals
        for (int i =0; i < animals.length; i++){
            animals[i].makeSound();
            animals[i].walk();
        }

    }
}
