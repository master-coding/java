package com.mastercoding.fullstack.java.classesobjects.abstraction.room;

public class Fan implements ElectricalDevice{
    @Override
    public void on() {
        System.out.println("Fan On");
    }

    @Override
    public void off() {
        System.out.println("Fan off");
    }
}
