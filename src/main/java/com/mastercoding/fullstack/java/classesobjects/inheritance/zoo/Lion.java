package com.mastercoding.fullstack.java.classesobjects.inheritance.zoo;

public class Lion extends Animal{
    @Override
    public void makeSound() {
        System.out.println("Lion is roaring");
    }
}
