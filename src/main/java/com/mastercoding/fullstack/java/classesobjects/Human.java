package com.mastercoding.fullstack.java.classesobjects;

public class Human {
    private float weight;
    private float height;
    private String colour;
    private String name;

    //constructors
    public Human(String name, float weight, float height, String colour){
        this.weight = weight;
        this.height = height;
        this.colour = colour;
        this.name = name;
    }

    public void walk() {
        System.out.println(this.name + " is walking");
    }

    public void stand() {
        System.out.println(this.name + " is standing");
    }

    public void talk() {
        System.out.println(this.name + " is talking");
    }

    public void listen(){
        System.out.println(this.name + " is listening");
    }

    public void updateName(String name) {
        this.name = name;
    }

}
