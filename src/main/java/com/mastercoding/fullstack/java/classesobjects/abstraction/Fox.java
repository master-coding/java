package com.mastercoding.fullstack.java.classesobjects.abstraction;

public class Fox implements Animal{
    @Override
    public void makeSound() {
        System.out.println("Fox is making sound");
    }

    @Override
    public void walk() {
        System.out.println("Fox is walking");
    }

    @Override
    public void run() {
        System.out.println("Fox is running");
    }
}
