package com.mastercoding.fullstack.java.classesobjects.room;

public class Fan {
    private String state = "OFF";

    public void start() {
        state = "RUNNING";
    }

    public void stop() {
        state = "OFF";
    }

    public void print() {
        System.out.println("Fan : " + state);
    }
}
