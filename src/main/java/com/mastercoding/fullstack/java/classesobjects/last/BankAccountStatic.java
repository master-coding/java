package com.mastercoding.fullstack.java.classesobjects.last;

public class BankAccountStatic {
    private static int totalTransactions = 0;
    private int numTransactions = 0;

    public void withdraw() {
        totalTransactions++;
        numTransactions++;
    }

    public void deposit() {
        totalTransactions++;
        numTransactions++;
    }

    public static void printTransactions(){
        System.out.println(totalTransactions);
    }
}
