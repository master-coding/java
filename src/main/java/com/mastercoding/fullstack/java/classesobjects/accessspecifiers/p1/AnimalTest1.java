package com.mastercoding.fullstack.java.classesobjects.accessspecifiers.p1;

public class AnimalTest1 {
    public static void main(String[] args) {
        Animal animal = new Animal();
        System.out.println(animal.noOfLegs);
        System.out.println(animal.weight);
        System.out.println(animal.color);

        animal.printColor();
        animal.printLegs();
        animal.printWight();
    }
}
