package com.mastercoding.fullstack.java.classesobjects.abstraction.room;

public class Light implements ElectricalDevice{
    @Override
    public void on() {
        System.out.println("Light On");
    }

    @Override
    public void off() {
        System.out.println("Light off");
    }
}
