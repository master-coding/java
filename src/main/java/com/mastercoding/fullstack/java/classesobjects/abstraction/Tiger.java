package com.mastercoding.fullstack.java.classesobjects.abstraction;

public class Tiger implements Animal{
    @Override
    public void makeSound() {
        System.out.println("Tiger is roaring");
    }

    @Override
    public void walk() {
        System.out.println("Tiger is walking");
    }

    @Override
    public void run() {
        System.out.println("Tiger is running");
    }
}
