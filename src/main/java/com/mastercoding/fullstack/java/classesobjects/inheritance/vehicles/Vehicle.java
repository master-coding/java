package com.mastercoding.fullstack.java.classesobjects.inheritance.vehicles;

public class Vehicle {
    public void start(){
        System.out.println("Vehicle is started");
    }

    public void stop() {
        System.out.println("Vehicle is stopped");
    }

    public void move() {
        System.out.println("Vehicle is moving");
    }
}
