package com.mastercoding.fullstack.java.classesobjects;

public class A {
    private int i;
    public int j;

    public A(){
        i = 10;
        j = 20;
    }

    public void print() {
        print1();
    }

    private void print1() {
        System.out.println(i);
        System.out.println(j);
    }
}
