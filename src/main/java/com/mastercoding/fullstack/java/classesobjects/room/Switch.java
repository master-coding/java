package com.mastercoding.fullstack.java.classesobjects.room;

public class Switch {
    private String state = "OFF";
    private Light light;
    private Fan fan;

    public void on() {
        if(light != null){
            light.glow();
        }
        if(fan != null){
            fan.start();
        }
        state = "ON";
    }

    public void off() {
        if(light != null){
            light.off();
        }
        if(fan != null){
            fan.stop();
        }

        state = "OFF";
    }

    public void connect(Light light){
        this.light = light;
    }

    public void connect(Fan fan){
        this.fan = fan;
    }

    public void print() {
        System.out.println("Switch: " + state);
    }
}
