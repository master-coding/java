package com.mastercoding.fullstack.java.classesobjects.last;

public class LionFinal extends AnimalFinal{
    @Override
    public void makeSound() {
        System.out.println("Animal is making sound");
    }
    @Override
    public void walk() {
        System.out.println("Animal is walking");
    }
}
