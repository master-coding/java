package com.mastercoding.fullstack.java.classesobjects.last;

public class FinalExample {
    public static void main(String[] args) {
        final float PI = 3.14f;

//        PI = 5.5f; compilation error
        System.out.println(PI);

    }
}
