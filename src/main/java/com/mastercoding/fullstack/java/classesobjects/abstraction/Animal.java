package com.mastercoding.fullstack.java.classesobjects.abstraction;

public interface Animal {
    void makeSound();
    void walk();
    void run();
}
