package com.mastercoding.fullstack.java.classesobjects.inheritance.vehicles;

public class Car extends Vehicle{
    @Override
    public void move(){
        System.out.println("Car is moving");
    }
}
