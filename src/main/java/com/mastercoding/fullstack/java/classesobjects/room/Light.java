package com.mastercoding.fullstack.java.classesobjects.room;

public class Light {
    private String state = "OFF";

    public void glow() {
        state = "GLOW";
    }

    public void off() {
        state = "OFF";
    }

    public void print() {
        System.out.println("Light : " + state);
    }
}
