package com.mastercoding.fullstack.java.classesobjects.abstraction;

public class Lion implements Animal {
    @Override
    public void makeSound() {
        System.out.println("Lion is roaring");
    }

    @Override
    public void walk() {
        System.out.println("Lion is walking");
    }

    @Override
    public void run() {
        System.out.println("Lion is running");
    }
}
