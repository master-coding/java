package com.mastercoding.fullstack.java.classesobjects.room;

public class SwitchBoard {

    private Switch[] switches = new Switch[5];
    private int i = 0;

    public void add(Switch swith){
        switches[i] = swith;
        i++;
    }

    public void operate(int index, String action){
        if(action.equals("ON")){
            switches[index].on();
        }else {
            switches[index].off();
        }
    }

    public void print() {
    }

}
