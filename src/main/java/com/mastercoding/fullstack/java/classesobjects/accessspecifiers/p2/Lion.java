package com.mastercoding.fullstack.java.classesobjects.accessspecifiers.p2;

import com.mastercoding.fullstack.java.classesobjects.accessspecifiers.p1.Animal;

public class Lion extends Animal{
    public void print() {
        printColor();
        printLegs();
    }
}
