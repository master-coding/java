package com.mastercoding.fullstack.java.classesobjects.inheritance.zoo;

public class Hyena extends Animal{
    @Override
    public void makeSound() {
        System.out.println("Hyena is laughing");
    }
}
