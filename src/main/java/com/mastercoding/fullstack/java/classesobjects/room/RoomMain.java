package com.mastercoding.fullstack.java.classesobjects.room;

public class RoomMain {
    public static void main(String[] args) {
        Switch s1 = new Switch();
        Light l1 = new Light();
        s1.connect(l1);

        Switch s2 = new Switch();
        Fan f1 = new Fan();
        s2.connect(f1);

        SwitchBoard switchBoard = new SwitchBoard();
        switchBoard.add(s1);
        switchBoard.add(s2);

        switchBoard.operate(0, "ON");
        switchBoard.operate(1, "ON");



    }
}
