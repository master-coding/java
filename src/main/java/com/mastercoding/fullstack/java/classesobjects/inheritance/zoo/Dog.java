package com.mastercoding.fullstack.java.classesobjects.inheritance.zoo;

public class Dog extends Animal {
    @Override
    public void makeSound() {
        System.out.println("Dog is barking");
    }
}
