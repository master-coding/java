package com.mastercoding.fullstack.java.classesobjects.abstraction.room;

public interface ElectricalDevice {
    void on();
    void off();
}
