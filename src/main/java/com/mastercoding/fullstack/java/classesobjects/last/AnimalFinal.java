package com.mastercoding.fullstack.java.classesobjects.last;

public class AnimalFinal {
    public void makeSound() {
        System.out.println("Animal is making sound");
    }
    public void walk() {
        System.out.println("Animal is walking");
    }
}
