package com.mastercoding.fullstack.java.classesobjects.inheritance.vehicles;

public class VehicleTest {
    public static void main(String[] args) {
        Car car = new Car();
        car.start();
        car.move();
        car.stop();

        System.out.println("--------------");
        Vehicle v1 = new Car();
        v1.start();
        v1.move();
        v1.stop();

        System.out.println("--------------");
        Vehicle v2 = new Bus();
        v2.start();
        v2.move();
        v2.stop();

        System.out.println("----------------");
        Vehicle[] vehicles = new Vehicle[2];
        vehicles[0] = v1;
        vehicles[1] = v2;

        for(int i=0; i < vehicles.length; i++){
            vehicles[i].start();
            vehicles[i].move();
            vehicles[i].stop();
        }


    }
}
