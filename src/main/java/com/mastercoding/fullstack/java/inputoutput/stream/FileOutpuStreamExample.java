package com.mastercoding.fullstack.java.inputoutput.stream;

import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutpuStreamExample {
    public static void main(String[] args) throws IOException {
        try(FileOutputStream outputStream = new FileOutputStream("./test.txt")){
            int i;
            while ((i = System.in.read()) != -1){
                outputStream.write(i);
            }
        }
    }
}
