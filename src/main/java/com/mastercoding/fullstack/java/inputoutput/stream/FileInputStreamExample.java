package com.mastercoding.fullstack.java.inputoutput.stream;

import java.io.FileInputStream;
import java.io.IOException;

public class FileInputStreamExample {
    public static void main(String[] args) throws IOException {
        try(FileInputStream inputStream = new FileInputStream("./build.gradle")){
            int ch;
            while ((ch = inputStream.read()) != -1){
                System.out.print((char)ch);
            }
        }
    }
}
