package com.mastercoding.fullstack.java.inputoutput.readerwriter;

import java.io.*;

public class FileWriterExample {
    public static void main(String[] args) throws IOException {
        try(FileWriter fileWriter = new FileWriter("./test.txt")) {
            fileWriter.write("we are testing\n");
            fileWriter.write("we are testing1");
        }

    }
}
