package com.mastercoding.fullstack.java.inputoutput.stream;

import java.io.BufferedInputStream;
import java.io.IOException;

public class BufferedInputStreamExample {
    public static void main(String[] args) throws IOException {
        BufferedInputStream inputStream = new BufferedInputStream(System.in);

        try{
            int ch;
            do {
                ch = inputStream.read();
                System.out.println((char) ch);
            } while (ch != -1);
        }
        finally {
            inputStream.close();
        }


//        BufferedInputStream inputStream = null;
//        try {
//            inputStream = new BufferedInputStream(System.in);
//            int ch;
//            while ((ch = inputStream.read()) != -1) {
//                System.out.print((char) ch);
//            }
//        } finally {
//            if (inputStream != null) {
//                inputStream.close();
//            }
//        }

    }
}
