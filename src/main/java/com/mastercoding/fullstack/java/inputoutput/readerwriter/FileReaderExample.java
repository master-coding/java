package com.mastercoding.fullstack.java.inputoutput.readerwriter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileReaderExample {
    public static void main(String[] args) throws IOException {
        try(BufferedReader inputStream = new BufferedReader(new FileReader("./build.gradle"))) {
            String line;
            while ((line = inputStream.readLine()) != null) {
                System.out.println(line);
            }
        }
    }
}
