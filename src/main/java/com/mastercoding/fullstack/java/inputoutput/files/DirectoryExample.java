package com.mastercoding.fullstack.java.inputoutput.files;

import java.io.File;

public class DirectoryExample {
    public static void main(String[] args) {
        File folder = new File(".");
        File files[] = new File[0];
        if(folder.isDirectory()){
            files = folder.listFiles();
        }
        for(File file : files){
            if(file.isFile()){
                System.out.println(file.getName());
            }
        }
    }
}
