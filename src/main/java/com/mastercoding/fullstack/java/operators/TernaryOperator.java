package com.mastercoding.fullstack.java.operators;

public class TernaryOperator {
    public static void main(String[] args) {
        int i = 10;
        int j = 20;

        if(i > j){
            System.out.println("The larger number is " + i);
        }else{
            System.out.println("The larger number is " + j);
        }

        int larger = i > j ? i : j;
        System.out.println("The larger number is " + larger);
    }
}
