package com.mastercoding.fullstack.java.evening;

import java.util.LinkedList;
import java.util.Queue;

public class QueueExample {
    public static void main(String[] args) {
        Queue<Float> q = new LinkedList<>();
        q.add(10.5f);
        q.add(20.1f);
        q.add(30.3f);

        System.out.println(q.peek());
        System.out.println(q.poll());
        System.out.println(q.poll());
        System.out.println(q.poll());
        System.out.println(q.poll());
    }
}
