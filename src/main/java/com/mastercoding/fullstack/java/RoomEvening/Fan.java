package com.mastercoding.fullstack.java.RoomEvening;

public class Fan {
    private String state = "OFF";

    public void start() {
        state = "ON";
    }

    public void stop() {
        state = "OFF";
    }

    public void print() {
        System.out.println("Fan : " + state);
    }
}
