package com.mastercoding.fullstack.java.exceptions;

public class BankAccount {
    private float balance = 5000;

    public void withdraw(float money) throws InSufficientFundsException {
        if(balance < money){
            throw new InSufficientFundsException();
        }
        balance = balance - money;
    }

    public void deposit(float money){
        if(money > 50000){
            throw new LimitExceededException();
        }
        balance = balance + money;
    }
}
