package com.mastercoding.fullstack.java.datatypes;

public class DataType {
    public static void main(String[] args) {
        int i = 10;
        short j = 20;
        long k = -30;

        float f = 2.3f;
        double d = 2.3;

        char c = 'a';
        byte b = 100;

        boolean b1 = true;
        String s = "Hello World!!";

        System.out.println(i);
        System.out.println(j);
        System.out.println(k);
        System.out.println(f);
        System.out.println(d);
        System.out.println(c);
        System.out.println(b);
        System.out.println(b1);
        System.out.println(s);
    }
}
