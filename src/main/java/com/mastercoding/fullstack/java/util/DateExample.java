package com.mastercoding.fullstack.java.util;

import java.util.Calendar;
import java.util.Date;

public class DateExample {
    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(date.toString());
        Calendar rightNow = Calendar.getInstance();
        System.out.println(rightNow);
    }
}
