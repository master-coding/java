package com.mastercoding.fullstack.java.packages;

public class IntegerExamples {
    public static void main(String[] args) {
        System.out.println(Integer.MAX_VALUE);

        Integer i = new Integer(100);
        int j = i.intValue();
        long k = i.longValue();

        String s = "ABC";
        int l = Integer.parseInt(s);
        System.out.println(l);
    }
}
