package com.mastercoding.fullstack.java.threads;

public class CounterThreads implements Runnable{
    private static Counter counter = new Counter();

    public void run() {
        for (int i=0; i < 100000; i++){
            counter.increment();
//            counter.decrement();
        }
    }

    public static void print() {
        System.out.println(counter.getCount());
    }
}
