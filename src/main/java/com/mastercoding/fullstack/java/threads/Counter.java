package com.mastercoding.fullstack.java.threads;

public class Counter {
    private int count=0;

    synchronized public void increment() {
            count++;
    }

    public void decrement() {
        count--;
    }

    public int getCount() {
        return count;
    }
}
