package com.mastercoding.fullstack.java.threads;

public class CounterTest {
    public static void main(String[] args) throws InterruptedException {
        CounterThreads c1 = new CounterThreads();
        Thread t1 = new Thread(c1);
        t1.start();
        CounterThreads c2 = new CounterThreads();
        Thread t2 = new Thread(c2);
        t2.start();
        CounterThreads c3 = new CounterThreads();
        Thread t3 = new Thread(c3);
        t3.start();

        t1.join();
        t2.join();
        t3.join();
        CounterThreads.print();
    }
}
