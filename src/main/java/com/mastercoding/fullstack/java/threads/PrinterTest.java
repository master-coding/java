package com.mastercoding.fullstack.java.threads;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class PrinterTest {
     public static void main(String[] args) {
        new Thread(new PrinterThread("./build.gradle")).start();
        new Thread(new PrinterThread("./build.gradle")).start();
        new Thread(new PrinterThread("./build.gradle")).start();
        new Thread(new PrinterThread("./build.gradle")).start();
    }
}

class Printer {
    synchronized static void print(String filePath) throws InterruptedException, IOException {
        Thread.sleep(200);
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(Thread.currentThread().getId() + " " + line);
            }
        }
    }
}

class PrinterThread implements Runnable {
    private String fileName;

    public PrinterThread(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void run() {
        try {
            Printer.print(fileName);
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getId() + " interrupted");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}