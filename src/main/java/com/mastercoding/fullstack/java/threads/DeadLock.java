package com.mastercoding.fullstack.java.threads;

public class DeadLock implements Runnable {

    final static Integer i = 10;
    final static Integer j = 20;
    private boolean order;

    public DeadLock(boolean order) {
        this.order = order;
    }

    public static void main(String[] args) {
        new Thread(new DeadLock(true)).start();
        new Thread(new DeadLock(false)).start();

    }

    @Override
    public void run() {
        try {
            if (order) {
                synchronized (i) {
                    Thread.sleep(5000);
                    synchronized (j) {
                        System.out.println(i + j);
                    }
                }
            } else {
                synchronized (j) {
                    Thread.sleep(5000);
                    synchronized (i) {
                        System.out.println(i + j);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
