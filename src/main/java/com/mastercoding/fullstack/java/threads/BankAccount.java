package com.mastercoding.fullstack.java.threads;

public class BankAccount extends Thread{
    public void run() {
        System.out.println("Thread is running :" + this.getId());
    }

    public static void main(String[] args) {
        BankAccount t1 = new BankAccount();
        t1.start();

        BankAccount t2 = new BankAccount();
        t2.start();

        BankAccount t3 = new BankAccount();
        t3.start();
    }
}
