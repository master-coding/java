package com.mastercoding.fullstack.java.sorting;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortExample {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(10);
        numbers.add(15);
        numbers.add(1);
        numbers.add(30);
        numbers.add(20);

        Collections.sort(numbers);
        System.out.println(numbers);

        List<String> names = new ArrayList<>();
        names.add("RAM");
        names.add("LAKSHMAN");
        names.add("BHARATH");

        Collections.sort(names);
        System.out.println(names);

        Person ram = new Person("RAM", "DD1", "HYD");
        Person lakshman = new Person("LAKSHMAN", "AA2", "HYD");
        Person bharath = new Person("BHARATH", "CC1", "HYD");
        Person john = new Person("JOHN", "KK1", "HYD");
        List<Person> personList = new ArrayList<>();
        personList.add(ram);
        personList.add(lakshman);
        personList.add(bharath);
        personList.add(john);

        Collections.sort(personList, new ComparePersonByName());
        System.out.println(personList);


        Person ram1 = new Person("RAM", "DD1", "HYD");
        Person lakshman1 = new Person("LAKSHMAN", "AA2", "HYD");
        Person bharath1 = new Person("BHARATH", "CC1", "HYD");
        Person john1 = new Person("JOHN", "KK1", "HYD");
        List<Person> personList1 = new ArrayList<>();
        personList1.add(ram1);
        personList1.add(lakshman1);
        personList1.add(bharath1);
        personList1.add(john1);

        Collections.sort(personList1, new ComparePersonByAaadharNumber());
        System.out.println(personList1);

    }
}
