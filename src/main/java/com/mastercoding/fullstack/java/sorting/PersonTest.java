package com.mastercoding.fullstack.java.sorting;

import java.util.HashMap;
import java.util.Map;

public class PersonTest {
    public static void main(String[] args) {
        Person ram = new Person("RAM", "A1111", "Hydreabad");
        Person ram2 = new Person("RAM", "A1111", "Hydreabad");
        Person ram1 = ram;
        System.out.println(ram);
        System.out.println(ram.equals(ram1));
        System.out.println(ram.equals(ram2));

        Map<Person, Float> personSalaryMap = new HashMap();
        personSalaryMap.put(ram, 100000f);
        personSalaryMap.put(ram2, 500f);

        System.out.println(personSalaryMap.get(ram));

    }
}
