package com.mastercoding.fullstack.java.sorting;

public class Person {
    private String name;
    private String aadharNumber;
    private String location;

    public Person(String name, String aadharNumber, String location) {
        this.name = name;
        this.aadharNumber = aadharNumber;
        this.location = location;
    }

    @Override
    public String toString() {
     return name + ":" + aadharNumber + ":" + location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        return aadharNumber != null ? aadharNumber.equals(person.aadharNumber) : person.aadharNumber == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (aadharNumber != null ? aadharNumber.hashCode() : 0);
        return result;
    }

    public String getName() {
        return name;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }


}
