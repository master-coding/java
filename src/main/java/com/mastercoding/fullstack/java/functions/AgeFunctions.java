package com.mastercoding.fullstack.java.functions;

import java.util.Scanner;

public class AgeFunctions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int age1 = scanner.nextInt();
        int age2 = scanner.nextInt();
        int age3 = scanner.nextInt();

        printAgeGroup(age1);
        printAgeGroup(age2);
        printAgeGroup(age3);

    }

    private static void printAgeGroup(int age){
        if(age >=0 && age <=12){
            System.out.println("Child");
        }else if(age >=13 && age <= 30){
            System.out.println("Young");
        }else if(age >=31 && age <= 50){
            System.out.println("Middle");
        }else if(age >=51){
            System.out.println("Senior");
        }
    }
}
