package com.mastercoding.fullstack.java.functions;

import java.util.Scanner;

public class AgeFunctionsReturnType {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int age1 = scanner.nextInt();
        int age2 = scanner.nextInt();
        int age3 = scanner.nextInt();

        String ageOneGroup = getAgeGroup(age1);
        System.out.println("age 1: " + age1 + ": " + ageOneGroup);
        String ageTwoGroup = getAgeGroup(age2);
        System.out.println("age 2: " + age2 + ": " + ageTwoGroup);
        String ageThreeGroup = getAgeGroup(age3);
        System.out.println("age 3: " + age3 + ": " + ageThreeGroup);

    }

    private static String getAgeGroup(int age){
        String ageGroup = "";
        if(age >=0 && age <=12){
            ageGroup = "Child";
        }else if(age >=13 && age <= 30){
            ageGroup = "Young";
        }else if(age >=31 && age <= 50){
            ageGroup = "Middle";
        }else if(age >=51){
            ageGroup = "Senior";
        }
        return ageGroup;
    }
}
