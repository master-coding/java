package com.mastercoding.fullstack.java.functions;

public class BMISmallerFunction {
    public static void main(String[] args) {
        String bmi = bmiString(52f, 63);
        System.out.println(bmi);

        System.out.println(cmToMeter(250));
        System.out.println(inchToMeter(250));
        System.out.println(bmi(52f, 63));
    }

    private static String bmiString(float weightKg, float heightInches){
        float bmiNumber = bmi(weightKg, heightInches);
        if(bmiNumber < 18.5){
            return "UNDER WEIGHT";
        }else if(bmiNumber < 24.9){
            return "NORMAL WEIGHT";
        }else if(bmiNumber < 29.9){
            return "OVER WEIGHT";
        }else {
            return "OBESE";
        }
    }

    private static float bmi(float weightKg, float heightInches) {
        float heightMeters = inchToMeter(heightInches);
        float bmiNumber = weightKg / (heightMeters * heightMeters);
        return bmiNumber;
    }
    private static float inchToMeter(float inches){
        float cms = inchToCentiMeter(inches);
        float meters = cmToMeter(cms);
        return meters;
    }

    private static float cmToMeter(float cms) {
        return cms/100f;
    }

    private static float inchToCentiMeter(float inches){
        float cms = inches * 2.53f;
        return cms;
    }
}
