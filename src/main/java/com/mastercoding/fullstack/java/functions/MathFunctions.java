package com.mastercoding.fullstack.java.functions;

public class MathFunctions {
    public static void main(String[] args) {
        int c = sum(20, 30);
        System.out.println(c);
        System.out.println(sum(100, 200));

        float p1 = percentage(5000.0f, 10.20f);
        System.out.println(p1);
        float p2 = percentage(5000.0f, 20.20f);
        System.out.println(p2);
        float p3 = percentage(5000.0f, 30.20f);
        System.out.println(p3);

    }

    private static int sum(int a, int b) {
        int c = a+b;
        return c;
    }

    private static float percentage(float number, float percentage){
        float per = (number * percentage)/100.0f;
        return per;
    }
}
