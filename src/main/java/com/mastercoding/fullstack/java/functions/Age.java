package com.mastercoding.fullstack.java.functions;

import java.util.Scanner;

public class Age {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int age1 = scanner.nextInt();
        int age2 = scanner.nextInt();
        int age3 = scanner.nextInt();

        if(age1 >=0 && age1 <=12){
            System.out.println("Child");
        }else if(age1 >=13 && age1 <= 30){
            System.out.println("Young");
        }else if(age1 >=31 && age1 <= 50){
            System.out.println("Middle");
        }else if(age1 >=51){
            System.out.println("Senior");
        }

        if(age2 >=0 && age2 <=12){
            System.out.println("Child");
        }else if(age2 >=13 && age2 <= 30){
            System.out.println("Young");
        }else if(age2 >=31 && age2 <= 50){
            System.out.println("Middle");
        }else if(age2 >=51){
            System.out.println("Senior");
        }

        if(age3 >=0 && age3 <=12){
            System.out.println("Child");
        }else if(age3 >=13 && age3 <= 30){
            System.out.println("Young");
        }else if(age3 >=31 && age3 <= 50){
            System.out.println("Middle");
        }else if(age3 >=51){
            System.out.println("Senior");
        }
    }
}
