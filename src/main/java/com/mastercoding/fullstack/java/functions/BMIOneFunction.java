package com.mastercoding.fullstack.java.functions;

public class BMIOneFunction {
    public static void main(String[] args) {
        String bmi = bmiString(52f, 63);
        System.out.println(bmi);
//
//        System.out.println(cmToMeter(250));
//        System.out.println(inchToMeter(250));
//        System.out.println(bmi(52f, 63));
    }

    private static String bmiString(float weightKg, float heightInches){
        float cms = heightInches * 2.53f;
        float heightMeters = cms/100f;
        float bmiNumber = weightKg / (heightMeters * heightMeters);

        if(bmiNumber < 18.5){
            return "UNDER WEIGHT";
        }else if(bmiNumber < 24.9){
            return "NORMAL WEIGHT";
        }else if(bmiNumber < 29.9){
            return "OVER WEIGHT";
        }else {
            return "OBESE";
        }
    }
}
