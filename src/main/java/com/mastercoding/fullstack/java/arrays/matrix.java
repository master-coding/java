package com.mastercoding.fullstack.java.arrays;

public class matrix {

    public static void main(String[] args) {
        int[][] m1 = new int[3][3];
        m1[0][0] = 1;
        m1[0][1] = 2;
        m1[0][2] = 3;
        m1[1][0] = 4;
        m1[1][1] = 5;
        m1[1][2] = 6;
        m1[2][0] = 7;
        m1[2][1] = 8;
        m1[2][2] = 9;

        int[][] m2 = {{10, -1, 20}, {30,40,50}, {1,2,3}};

        for (int r=0; r < m1.length; r++){
            for(int c=0; c < m1[0].length; c++){
                System.out.print(m1[r][c] + " ");
            }
            System.out.println("");
        }

        System.out.println("--------");
        for (int r=0; r < m2.length; r++){
            for(int c=0; c < m2[0].length; c++){
                System.out.print(m2[r][c] + " ");
            }
            System.out.println("");
        }

        System.out.println("Adding two matrix");

        int[][] sumMatrix = new int[3][3];

        for(int r=0; r < sumMatrix.length; r++){
            for(int c=0; c < sumMatrix[0].length; c++){
                sumMatrix[r][c] = m1[r][c] + m2[r][c];
            }
        }

        for (int r=0; r < sumMatrix.length; r++){
            for(int c=0; c < sumMatrix[0].length; c++){
                System.out.print(sumMatrix[r][c] + " ");
            }
            System.out.println("");
        }

    }
}
