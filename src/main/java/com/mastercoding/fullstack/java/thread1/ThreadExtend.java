package com.mastercoding.fullstack.java.thread1;

public class ThreadExtend extends Thread{
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getId() + ": " + " Hello");
    }
}
