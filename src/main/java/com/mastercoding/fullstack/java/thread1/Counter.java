package com.mastercoding.fullstack.java.thread1;

public class Counter {
    private int i = 0;

    synchronized public void increment() {
        i = i + 1;
    }

    public void decrement() {
        i = i - 1;
    }

    @Override
    public String toString() {
        return "Counter{" +
                "i=" + i +
                '}';
    }
}
