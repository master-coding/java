package com.mastercoding.fullstack.java.thread1;

public class CounterThread implements Runnable{
    private static Counter counter = new Counter();
    @Override
    public void run() {
        for(int i=0; i < 10000; i++){
            counter.increment();
        }
    }

    public static void print() {
        System.out.println(counter.toString());
    }
}
