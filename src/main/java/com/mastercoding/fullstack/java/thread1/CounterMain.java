package com.mastercoding.fullstack.java.thread1;

public class CounterMain{
    public static void main(String[] args) throws InterruptedException {
        CounterThread ct1 = new CounterThread();
        Thread t1 = new Thread(ct1);
        t1.start();

        CounterThread ct2 = new CounterThread();
        Thread t2 = new Thread(ct2);
        t2.start();

        t1.join();
        t2.join();
        CounterThread.print();
    }
}
