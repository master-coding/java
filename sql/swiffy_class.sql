drop database swiggy;

create DATABASE IF NOT EXISTS swiggy;

create TABLE swiggy.restaurant(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 	name VARCHAR(50) NOT NULL, area VARCHAR(100) nOT NULL, open_time TIME, close_time TIME,  country VARCHAR(100) default 'INDIA',
UNIQUE KEY(name, area));

-- INSERT INTO swiggy.restaurant(name, area, open_time, close_time) values('SWAGAT', 'MIYAPUR', '10:30:00', '23:00:00');

-- INSERT INTO swiggy.restaurant(name, area, open_time, close_time) values('SWAGAT', 'MIYAPUR', '10:30:00', '23:00:00');

SELECT * FROM swiggy.restaurant;

ALTER TABLE swiggy.restaurant DROP KEY name;

DELETE FROM swiggy.restaurant WHERE ID=3;

ALTER TABLE swiggy.restaurant ADD unique KEY uk_name_area (name, area);

CREATE TABLE swiggy.review (
  id INT NOT NULL,
  restaurant_id INT NOT NULL,
  comment VARCHAR(100),
  rating decimal(1,1) NOT NULL,
  PRIMARY KEY (id),
  foreign key fk_restaurant_id (restaurant_id) references swiggy.restaurant(id));
  
  drop table swiggy.review;
  
  CREATE TABLE swiggy.customer(id INT NOT NULL AUTO_INCREMENT primary key, name VARCHAR(50) not null);
  
  -- alter table swiggy.customer ADD PRIMARY KEY(id);
  
  create table swiggy.review(time TIMESTAMP, customer_id INT, restaurant_id INT, 
  comment VARCHAR(100), rating DOUBLE(2,1),
  primary key(time, customer_id, restaurant_id),
  foreign key fk_customer_id(customer_id) references swiggy.customer(id),
  foreign key fk_restaurant_id(restaurant_id) references swiggy.restaurant(id)
  );

create table swiggy.item(id int not null primary key AUTO_INCREMENT, name varchar(50) not null);

create table swiggy.item_restaurant(item_id int not null, restaurant_id int not null, 
price float, prepare_time TIME,
primary key(item_id, restaurant_id),
foreign key fk_item_id(item_id) references swiggy.item(id),
foreign key fk_restaurant_id(restaurant_id) references swiggy.restaurant(id));

create table swiggy.delivery_boy(id int not null primary key AUTO_INCREMENT, name varchar(50));

create table swiggy.swiggy_order(id int not null primary key AUTO_INCREMENT, customer_id int not null, restaurant_id int not null, 
delivery_boy_id int not null, booking_time TIMESTAMP, status varchar(50),
total_price float,
foreign key fk_customer_id(customer_id) references swiggy.customer(id),
foreign key fk_resaurant_id(restaurant_id) references swiggy.restaurant(id),
foreign key fk_delivery_boy_id(delivery_boy_id) references swiggy.delivery_boy(id));

create table swiggy.order_item(order_id int not null, item_id int not null,
foreign key fk_order_id(order_id) references swiggy.swiggy_order(id),
foreign key fk_item_id(item_id) references swiggy.item(id));












