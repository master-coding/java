-- get all restaurants
select * from swiggy.restaurant;

-- get the restaurants which are in Banjara Hills
select * from swiggy.restaurant where area = 'Banjara Hills';

-- get the restaurants which has area having the keyword Hills.
select * from swiggy.restaurant where area like '%Hills%';

-- get the name, open_time, close_time of restaurants which has area having the keyword Hills.
select name, area, open_time, close_time from swiggy.restaurant where area like '%Hills%';

-- select name, area of restuaraunts sorted in ascending order by restaurant name
select r.name, r.area from swiggy.restaurant r order by r.name;

-- select name, area of restuaraunts sorted in descending order by restaurant name
select r.name, r.area from swiggy.restaurant r order by r.name desc;

-- select name, area of restuaraunts sorted in order by area
select r.name, r.area from swiggy.restaurant r order by r.area;

-- get the restaurants which are in Banjara Hills and in Jubilee Hills
select * from swiggy.restaurant where area = 'Banjara Hills' or area = 'Jubilee Hills';
select * from swiggy.restaurant where area in ('Banjara Hills', 'Jubilee Hills');

-- get the restaurants which are in Banjara Hills and in Jubilee Hills and which are open at 11:00
select * from swiggy.restaurant where area in ('Banjara Hills', 'Jubilee Hills') and open_time = '11:00';

-- get the restaurants which open after 10'o clock
select * from swiggy.restaurant where open_time >= '10:00';

-- get the total price of all the orders
select  sum(o.total_price) from swiggy.swiggy_order o;


select count(*) from swiggy.review r;
select count(*) from swiggy.customer c;
-- given customer get the name, comment and rating
select c.name, r.comment, r.rating from swiggy.review r join swiggy.customer c on c.id = r.customer_id
where c.name = 'lakshman';

-- get the restaurant name, item name, item price for all the restaurants
select r.name, i.name, ir.price from swiggy.item_restaurant ir join swiggy.item i join swiggy.restaurant r
on ir.item_id = i.id and ir.restaurant_id = r.id;

-- get the restaurant name and rating for all the matching records and unmatched records from restaurant
select r1.name, r2.rating from swiggy.restaurant r1 left outer join swiggy.review r2 on r1.id = r2.restaurant_id;

select r1.name, r2.rating from swiggy.review r2 right outer join swiggy.restaurant r1 on r1.id = r2.restaurant_id;

-- union all
select * from swiggy.restaurant r1 where r1.area like '%Hills'
UNION
select * from swiggy.restaurant r2 where r2.name = 'Swagat' or r2.name='Punjab Grill';

-- number of reviews and average rating given by a customer id
select r.customer_id, count(*) review_count, avg(rating) avg_rating from swiggy.review r group by r.customer_id;

-- describing the review table. 
desc swiggy.review;

-- number of reviews and average rating given by a customer id
select c.name, count(*) review_count, avg(rating) avg_rating from swiggy.review r join swiggy.customer c on r.customer_id = c.id group by c.name;

-- find the maximum number of reviews given by a single customer
select max(review_count) max_review_count from (
select c.name, count(*) review_count from swiggy.review r join swiggy.customer c on r.customer_id = c.id group by c.name
) T1;

-- get the customer ids who gave equal or more than 2 comments;
select r.customer_id, count(*) review_count from swiggy.review r group by r.customer_id having review_count >= 2;

-- get the customer names who gave equal or more than 2 comments;
select c.name from swiggy.customer c where c.id in (
select customer_id from (
select r.customer_id, count(*) review_count from swiggy.review r group by r.customer_id having review_count >= 2)  T1);

-- get the customer names for the customer ids availabel in review_count
select c.name from swiggy.customer c where  c.id in(select r.customer_id review_count from swiggy.review r);

-- get the customer names for the customer ids availabel in review_count
select c.name from swiggy.customer c join swiggy.review r on c.id = r.customer_id;

-- between
select * from swiggy.review where rating between 4 and 5;

-- case
select customer_id, rating, case when rating >= 4 then'excellment' when rating >= 3 then 'avg' else 'bad' end from swiggy.review;

select rating/10 from swiggy.review;

-- is null
insert into swiggy.review(time, customer_id, restaurant_id, comment, rating) values(current_timestamp(), 1, 1, null, 4);
select * from swiggy.review where comment is null;

-- mod
select rating % 3 from swiggy.review;

-- not equal
select * from swiggy.customer c where c.name != 'Ram';
select * from swiggy.customer c where c.name <> 'Ram';

-- if condition
select customer_id, rating, IF(rating >= 4, 'excellment', 'bad') from swiggy.review;

-- if null
SELECT customer_id, IFNULL(comment, 'comment not provider'), rating from swiggy.review;

-- concat
select concat('we', ' are ', 'class');

select concat(customer_id, ':', comment, ' : ', rating) from swiggy.review;
select concat_ws(':', customer_id, comment, rating) from swiggy.review;

-- length
select length(comment) from swiggy.review;

select lower(comment), upper(comment) from swiggy.review;

select date(time), time(r.time) from swiggy.review r;

-- distinct
select  distinct restaurant_id, r.rating from swiggy.review r order by r.restaurant_id, r.rating;
