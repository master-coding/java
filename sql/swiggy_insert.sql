-- restaurants
insert into swiggy.restaurant(name, area, open_time, close_time) values('AB\'s Absolute Barbecue', 'gachibowli', '11:00', '22:30');
insert into swiggy.restaurant(name, area, open_time, close_time) values('Almond House', 'Banjara Hills', '11:00', '22:30');
insert into swiggy.restaurant(name, area, open_time, close_time) values('Punjab Grill', 'Jubilee Hills', '11:00', '22:30');
insert into swiggy.restaurant(name, area) values('TGI Fridays', 'Banjara Hills');
insert into swiggy.restaurant(name, area, open_time, close_time) values('swagat', 'Miyapur', '10:00', '22:00');

-- customers
insert into swiggy.customer(name) values('ram'), ('lakshman'), ('john'), ('bob'), ('bharath'), ('pop'), ('jenni');



-- reviews
insert into swiggy.review(time, customer_id, restaurant_id, comment, rating) values(current_timestamp(), 1, 1, 'Excellent', 4.5);
insert into swiggy.review(time, customer_id, restaurant_id, comment, rating) values(current_timestamp(), 3, 1, 'Good', 3.4);
insert into swiggy.review(time, customer_id, restaurant_id, comment, rating) values(current_timestamp(), 5, 1, 'average', 4);
insert into swiggy.review(time, customer_id, restaurant_id, comment, rating) values(current_timestamp(), 4, 1, 'super', 5);

insert into swiggy.review(time, customer_id, restaurant_id, comment, rating) values(current_timestamp(), 2, 3, 'Avg', 3);
insert into swiggy.review(time, customer_id, restaurant_id, comment, rating) values(current_timestamp(), 3, 3, 'Good', 3.5);
insert into swiggy.review(time, customer_id, restaurant_id, comment, rating) values(current_timestamp(), 7, 3, 'ok', 2);
insert into swiggy.review(time, customer_id, restaurant_id, comment, rating) values(current_timestamp(), 4, 3, 'very bad', 1.4);
insert into swiggy.review(time, customer_id, restaurant_id, comment, rating) values(current_timestamp(),2, 2, 'Good', 3.5);

insert into swiggy.item(name) values('chicken biryani'), ('franky'), ('veg biryani'), ('tandori chicken'), ('panner kaju'), ('sheek kabab');

insert into swiggy.item_restaurant values(1, 1, 300, '00:30'), (3, 1, 200, '00:25'), (6, 1, 250, '01:00');
insert into swiggy.item_restaurant values(2, 3, 100, '00:15'), (3, 3, 300, '00:30'), (5, 3, 300, '01:10');
insert into swiggy.item_restaurant values(1, 2, 200, '00:20'), (3, 2, 150, '00:20'), (5, 2, 150, '00:40');

insert into swiggy.delivery_boy(name) values('D1'),('D2'), ('D3'); 

insert into swiggy.swiggy_order(customer_id, restaurant_id, delivery_boy_id, booking_time, status, total_price)
values(1, 1, 1, '2018-08-21 19:30:30', 'order picked', 800);

insert into swiggy.swiggy_order(customer_id, restaurant_id, delivery_boy_id, booking_time, status, total_price)
values(3, 1, 2, '2018-08-20 18:30:30', 'order picked', 1000);

insert into swiggy.swiggy_order(customer_id, restaurant_id, delivery_boy_id, booking_time, status, total_price)
values(1, 1, 2, '2018-08-19 19:30:30', 'order picked', 2000);

insert into swiggy.swiggy_order(customer_id, restaurant_id, delivery_boy_id, booking_time, status, total_price)
values(2, 2, 2, '2018-08-16 20:30:30', 'order picked', 1500);

insert into swiggy.swiggy_order(customer_id, restaurant_id, delivery_boy_id, booking_time, status, total_price)
values(4, 2, 3, '2018-08-10 11:30:30', 'order delivered', 980);


insert into swiggy.swiggy_order(customer_id, restaurant_id, delivery_boy_id, booking_time, status, total_price)
values(4, 1, 1, '2018-08-14 12:30:30', 'order delivered', 600);

insert into swiggy.swiggy_order(customer_id, restaurant_id, delivery_boy_id, booking_time, status, total_price)
values(7, 5, 2, '2018-08-11 20:30:30', 'order pending', 1430);
